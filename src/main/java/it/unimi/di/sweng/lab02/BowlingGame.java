package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	public static int MAX_ROLLS = 21;
	private int [] rolls = new int [21]; 
	private int lancio = 0;

	@Override
	public void roll(int pins) {
			rolls[lancio] = pins;
			lancio++;
			if(pins == 10 && lancio%2==1 && lancio <= 20){
				rolls[lancio] = 0;
				if(lancio < 18)
					lancio++;
			}
	}

	public boolean isSpare(int posizione){
		if(posizione < 2)
			return false;
		else{
			if(posizione%2 == 0 &&rolls[posizione-1] + rolls[posizione-2] == 10)
				return true;
			else
				return false;
		} 
	}
	
	public boolean isStrike(int posizione){
		if(posizione < 2)
			return false;
		else{
			if(posizione%2 == 0 &&rolls[posizione-1] == 0 && rolls[posizione-2] == 10)
				return true;
			else
				return false;
		} 
	}
	
	@Override
	public int score() {
		int score = 0;
		for(int i = 0; i < MAX_ROLLS; i++){
			if(isStrike(i) && i == 18)
				score = rolls[i] + rolls[i++] +rolls[i++];
			else{
				if(isStrike(i))
					score += (rolls[i] + rolls[i++])*2;
				else if(isSpare(i))
					score += rolls[i]*2;
				else	
				    score += rolls[i];			
			}
		}
	return score;
	}


}